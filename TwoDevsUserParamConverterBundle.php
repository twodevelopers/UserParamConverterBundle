<?php

/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 22.01.2015
 * Time: 13:40
 */

namespace TwoDevs\UserParamConverterBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TwoDevsUserParamConverterBundle extends Bundle
{
}
