<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 22.01.2015
 * Time: 13:40
 */

namespace TwoDevs\UserParamConverterBundle\Request\ParamConverter;

use Doctrine\Common\Persistence\ManagerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DoctrineParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface as User;

class UserParamConverter extends DoctrineParamConverter implements ParamConverterInterface
{

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * @param ManagerRegistry $registry
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ManagerRegistry $registry = null, TokenStorageInterface $tokenStorage = null)
    {
        $this->tokenStorage = $tokenStorage;
        parent::__construct($registry);
    }

    public function supports(ParamConverter $configuration)
    {
        $parentReturn = parent::supports($configuration);

        if (!$parentReturn || null === $this->tokenStorage) {
            return $parentReturn;
        }

        return (is_subclass_of($configuration->getClass(), 'Symfony\Component\Security\Core\User\UserInterface'));
    }

    protected function getIdentifier(Request $request, $options, $name)
    {
        $id = parent::getIdentifier($request, $options, $name);

        if ('me' == mb_strtolower($id, 'UTF-8')) {
            $token = $this->tokenStorage->getToken();
            if (null !== $token) {
                if ($token->getUser() instanceof User) {
                    return $token->getUser()->getId();
                }
            }
        }

        return $id;
    }
}
